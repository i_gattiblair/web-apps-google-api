//REFERENCE 
//https://developers.google.com/maps/documentation/javascript/geolocation


var map, infoWindow;

function initMap() {

    infoWindow = new google.maps.InfoWindow;

    //takes the div 'map' in index.html and modifies it
    //to look like Google Maps by using the Google API
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            //This will show a message where the user is located
            //provided they allow website to see user's location
            infoWindow.setPosition(pos);
            infoWindow.setContent('This is Your Location.');
            infoWindow.open(map);
            map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });


        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6
        });


    } 
    else {
        


        handleLocationError(false, infoWindow, map.getCenter());


 
   }
} 
//this shows up if device does not have location available
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);

}