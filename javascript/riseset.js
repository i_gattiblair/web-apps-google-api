function sunriseSet(){
    
    // var currentDate = new Date().toISOString().slice(0, 10);
    var latitude = document.getElementById("latitude").innerHTML;
    var longitude = document.getElementById("longitude").innerHTML;

    var checkCoordinates = false;
    
    if(latitude === "" || longitude === ""){
        alert("You need to specify a Coordinate first");
    }

    else{
      checkCoordinates = true;
    }

    var url = "php/cURLriseset.php";
        var params = "?latitude=" + latitude + "&longitude=" + longitude;
        var targetSunrise = document.getElementById("sunrise");
        var targetSunset = document.getElementById("sunset");
        var xhr = new XMLHttpRequest();
        
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2 && checkCoordinates == true) {
            targetSunrise.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200 && checkCoordinates == true) {
            var risesetXHR = [JSON.parse(xhr.responseText)];
            var risesetResults = risesetXHR[0].results;
            
            document.getElementById('sunrise').innerHTML = risesetResults.sunrise + " (UTC)";
            document.getElementById('sunset').innerHTML = risesetResults.sunset + " (UTC)";



          }
        }
        xhr.open('GET', url + params, true);
        xhr.send();

     
}
