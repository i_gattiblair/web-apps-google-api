<?php


$city = $_GET['city'];
$region = $_GET['region'];
$search = $city . '&+' . $region;

$url = str_replace( ' ', '%20', "http://maps.googleapis.com/maps/api/geocode/json?address=". $search );

//turns user city/region input and uses cURL to gather the coordinates.
$string  = curl_init();
curl_setopt($string, CURLOPT_URL, $url);  
curl_setopt($string, CURLOPT_RETURNTRANSFER, 1 );
$result   = json_decode(curl_exec($string), true);


$geolocation = $result['results'][0]['geometry'];

//takes the longitude and latitude from the API and sets it in
//array, ready to be encoded for JSON 
$long_lat = array(
    'longitude' => $geolocation['location']['lng'],
    'latitude' => $geolocation['location']['lat']
);

//encodes array with coordinates and encodes it in JSON
//and sends it back to the JavaScript page.
$myjson = json_encode($long_lat);
print_r($myjson);
 


curl_close($string);
?>